# OrangeFox Recovery device tree for Samsung Galaxy S8+ aka dream2lte.

## How to build
1. Set up the build environment following instructions from [here](https://wiki.orangefox.tech/en/dev/building)
2. In the root folder of cloned repo you need to clone the device tree:
```bash
git clone https://gitlab.com/ksant0s/android_device_samsung_dream2lte.git -b fox9.0 device/samsung/dream2lte
```
3. To build:
```bash
. build/envsetup.sh && export ALLOW_MISSING_DEPENDENCIES=true && export LC_ALL="C" && lunch omni_dream2lte-eng && mka recoveryimage -j128
```
